<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;	
class carreraController extends Controller
{
	public function __construct()
	{

	}
    public function index (Request $request)
    {
    	$query=trim($request->get('searchText'));
    	return DB::table('carreras')
    	->where(function($q2) use ($query)
    	{
    		$q2->whereRaw('nombre_carrera COLLATE UTF8_GENERAL_CI like ?',array('%'.$query.'%'));
		})->get();
    	

    	
    	//('nombre_carrera','like','%'.$query.'%')->get();
    }
    public function create()
    {

    }
    public function store(Request $request)
    {

    }

}
