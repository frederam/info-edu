<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class evento extends Model
{
    //// Tabla en la base de datos
    protected $table = 'evento'; // Carrera es el nombre correcto
    public $incrementing = false;

    // Primary key
    protected $primaryKey = 'codigo';

    // No se guarde el tiempo y la fecha del registro
    public $timestamps = false;

    protected $fillable = [
    	'codigo', 'f_inicio', 'f_fin', 'es_hora_exacta','poster', 'descripcion','cod_recinto', 'institucion', 'latitud', 'longitud', 'titulo'
    ];

    protected $guarded = []; // Vacío
}
